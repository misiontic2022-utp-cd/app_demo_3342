import 'package:flutter/material.dart';

import 'page1.dart';

class PageTwo extends StatelessWidget {
  final String text;

  const PageTwo({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pagina Dos"),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(text),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              child: const Text("Volver a la pagina 1"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            const SizedBox(
              height: 8,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PageOne(),
                  ),
                );
              },
              child: const Text("Abrir pagina 1"),
            )
          ],
        ),
      ),
    );
  }
}
