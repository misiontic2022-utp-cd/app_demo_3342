import 'package:app_demo_3342/pages/detail.dart';
import 'package:flutter/material.dart';

import 'page2.dart';

class PageOne extends StatelessWidget {
  const PageOne({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pagina Uno"),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ElevatedButton(
              child: const Text("Ir a pagina 2"),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const PageTwo(
                        text: "Mi nombre es Cesar",
                      ),
                    ));
              },
            ),
            ElevatedButton(
              child: const Text("Ver detalle"),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const DetailPage(
                        imageUrl:
                            "https://publimotos.com/images/2022/06-junio/junio-29/cali/cali-06-calima.jpg",
                        name: "Lago Calima",
                        location: "Calima Darien - Valle del Cauca, Colombia",
                        description:
                            "El embalse del Calima conocido como Lago Calima es uno de los embalses más grandes de América, con una superficie de 19.34 kilómetros cuadrados.​ Se encuentra en el municipio de Calima El Darién.",
                      ),
                    ));
              },
            ),
          ],
        ),
      ),
    );
  }
}
