import 'package:app_demo_3342/widgets/buttons.dart';
import 'package:flutter/material.dart';

import '../widgets/info.dart';

class DetailPage extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String location;
  final String description;

  const DetailPage(
      {super.key,
      required this.imageUrl,
      required this.name,
      required this.location,
      required this.description});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          Image.network(imageUrl),
          const SizedBox(
            height: 16,
          ),
          InfoWidget(
            name: name,
            location: location,
          ),
          const SizedBox(
            height: 16,
          ),
          const ButtonsWidget(),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(description),
          ),
        ],
      ),
    );
  }
}
