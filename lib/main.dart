import 'package:flutter/material.dart';

import 'pages/detail.dart';
import 'pages/home.dart';
import 'pages/page1.dart';

void main(List<String> args) {
  runApp(
    const MaterialApp(
      title: "Mi primera aplicacion",
      home: PageOne(),
    ),
  );
}
