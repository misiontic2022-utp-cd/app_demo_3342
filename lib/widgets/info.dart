import 'package:flutter/material.dart';

class InfoWidget extends StatefulWidget {
  final String name;
  final String location;

  const InfoWidget({super.key, required this.name, required this.location});

  @override
  State<InfoWidget> createState() => _InfoWidgetState();
}

class _InfoWidgetState extends State<InfoWidget> {
  bool _estaMarcado = false;
  int _numeroMarcas = 41;

  void _marcarComoFavorito() {
    setState(() {
      _numeroMarcas = _estaMarcado ? _numeroMarcas - 1 : _numeroMarcas + 1;
      _estaMarcado = !_estaMarcado;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Column(
            children: <Widget>[
              Text(
                widget.name,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                widget.location,
                style: const TextStyle(color: Color(0x55555588)),
              )
            ],
          ),
          const SizedBox(
            width: 15,
          ),
          IconButton(
            icon: Icon(_estaMarcado ? Icons.star : Icons.star_outline),
            color: Colors.red,
            onPressed: _marcarComoFavorito,
          ),
          Text("$_numeroMarcas")
        ],
      ),
    );
  }
}
