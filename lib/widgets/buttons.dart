import 'package:flutter/material.dart';

class ButtonsWidget extends StatelessWidget {
  const ButtonsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: const <IconTextWidget>[
        IconTextWidget(
          icon: Icons.call,
          texto: "Call",
        ),
        IconTextWidget(
          icon: Icons.route,
          texto: "Route",
        ),
        IconTextWidget(
          icon: Icons.share,
          texto: "Share",
        ),
      ],
    );
  }
}

class IconTextWidget extends StatelessWidget {
  final IconData icon;
  final String texto;

  const IconTextWidget({super.key, required this.icon, required this.texto});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Icon(icon),
        const SizedBox(
          height: 8,
        ),
        Text(texto.toUpperCase()),
      ],
    );
  }
}
